
// Exam1 Practical
// Simon M Schick

#include <iostream>
#include <conio.h>

using namespace std;

bool loop = true;
bool valid = false;
float numInput;
int exponentInput;
char again;

// protoyping our functions
void Square(float &answer);
void Cube(float &answer);

int main()
{
	while (loop == true)
	{
		valid = false;
		cout << "Please enter in a number (positive,negative, and/or decimal)\n";
		cin >> numInput;
		

		while(valid == false) // checks to see if you input a 2 or 3
		{
			cout << "\nNow please enter in a 2 for your number to be squared, or 3 to be cubed\n";
			cin >> exponentInput;

			if (exponentInput == 2 || exponentInput == 3)
			{
				valid = true;
			}
			else
			{
				cout << "\nYou did not input a 2 or 3!!!\n";
				valid = false;
			}
				
		}
		if (exponentInput == 2)
			Square(numInput);
		else if (exponentInput == 3)
			Cube(numInput);

		cout << "The answer is: " << numInput; // outputs the answer

		cout << "\n\nwould you like to go again? Y = yes - Any other key = no\n";
		cin >> again;
		((again == 'Y') || (again == 'y')) ? loop = true : loop = false; // checks if user wants to go again
		cout << "\n";
	}

	(void)_getch();
	return 0;
}

void Square(float& answer)
{
	answer *= answer;
}

void Cube(float& answer)
{
	answer = answer *= (answer * answer);
}